@extends('page.master')


@section('judul')

<h1>Buat Account Baru!</h1>

@endsection


@section('isi')
<h2>Sign Up Form</h2>
<form action="/welcome" method="POST">
    @csrf
    <label> First name: </label> <br><br>
    <input type="text" name="firstname"> <br><br>
    <label> Last name: </label> <br><br>
    <input type="text" name="lastname"> <br><br>
    <label> Gender: </label> <br><br>
    <input type="radio" name="G">Male <br>
    <input type="radio" name="G">Female <br>
    <input type="radio" name="G">Other <br><br>
    <label> Nationality: </label> <br><br>
    <select name="N">
        <option value="Indonesian">Indonesian</option>
        <option value="Malaysian">Malaysian</option>
        <option value="Singaporean">Singaporean</option>
        <option value="Australian">Australian</option>
    </select> <br><br>
    <label>Language Spoken: </label> <br><br>
    <input type="checkbox">Bahasa Indonesia <br>
    <input type="checkbox">English <br>
    <input type="checkbox">Other<br><br>
    <label>Bio: </label> <br><br>
    <textarea name="bio" cols="30" rows="10"></textarea> <br><br>
    <input type="submit" value="Sign Up">
</form>

@endsection

