<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function index(){
        $cast= DB::table('cast')->get();
        return view('page.index', compact('cast'));
    }

    public function create(){
        return view ('page.create');
    }

    public function store(Request $request){
        $request->validate([
            'NamaCast' => 'required|unique:post',
        
        ]);
        $query = DB::table('cast')->insert([
            "nama" => $request["NamaCast"],
        
        ]);
        return redirect('/cast');
    }
}
