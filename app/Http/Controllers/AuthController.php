<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view ('page.register');
    }

    public function kirim(Request $request){
        $firstname = $request->firstname;
        $lastname = $request->lastname;
        return view("page.welcome" , compact('firstname', 'lastname'));

    }

    public function table(){
        return view ('table.table');
    }

    public function datatable(){
        return view ('table.datatable');
    }
}
